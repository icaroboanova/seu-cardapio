package br.cairu.seucardapio.to;

public class FreteTO {

    private String neighborhood;
    private Double price;

    public FreteTO(String neighborhood, Double price) {
        this.neighborhood = neighborhood;
        this.price = price;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}
