package br.cairu.seucardapio.to;

import java.io.Serializable;
import java.util.List;

public class ProductTO implements Serializable {

    private static final long serialVersionUID = 7206145222874692321L;

    private int id;
    private String imageUrl;
    private String name;
    private String description;
    private Double price;
    private List<AdditionalOptionsTO> additionalOptions;

    public ProductTO(int id, String imageUrl, String name, String description, Double price) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public int getId() { return id; }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<AdditionalOptionsTO> getAdditionalOptions() { return additionalOptions; }

    public void setAdditionalOptions(List<AdditionalOptionsTO> additionalOptions) { this.additionalOptions = additionalOptions; }
}
