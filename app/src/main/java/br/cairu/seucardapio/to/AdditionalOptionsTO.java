package br.cairu.seucardapio.to;

import java.util.List;

public class AdditionalOptionsTO {

    private int id;
    private String name;
    private boolean check;
    private List<OptionTO> options;

    public AdditionalOptionsTO(int id, String name, boolean check, List<OptionTO> options) {
        this.id = id;
        this.name = name;
        this.check = check;
        this.options = options;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public List<OptionTO> getOptions() {
        return options;
    }

    public void setOptions(List<OptionTO> options) {
        this.options = options;
    }
}
