package br.cairu.seucardapio.to;

import android.widget.EditText;

public class EnderecoTO {

    String cep;
    String street;
    String neighborhood;
    String city;
    String number;
    String complement;
    String name;

    public EnderecoTO(String cep, String street, String neighborhood, String city, String number, String complement, String name) {
        this.cep = cep;
        this.street = street;
        this.neighborhood = neighborhood;
        this.city = city;
        this.number = number;
        this.complement = complement;
        this.name = name;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }
}
