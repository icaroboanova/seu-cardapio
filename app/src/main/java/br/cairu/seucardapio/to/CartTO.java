package br.cairu.seucardapio.to;

import java.util.List;

public class CartTO {

    List<CartProductTO> products;

    public CartTO(List<CartProductTO> products) {
        this.products = products;
    }

    public List<CartProductTO> getProducts() {
        return products;
    }

    public void setProducts(List<CartProductTO> products) {
        this.products = products;
    }

}
