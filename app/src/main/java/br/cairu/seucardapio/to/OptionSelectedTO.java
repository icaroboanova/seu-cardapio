package br.cairu.seucardapio.to;

public class OptionSelectedTO {

    private String name;
    private String value;
    private Double price;

    public OptionSelectedTO(String name, String value, Double price) {
        this.name = name;
        this.value = value;
        this.price = price;
    }

    public OptionSelectedTO(OptionTO option) {
        this.name = option.getName();
        this.value = option.getValue();
        this.price = option.getPrice();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
