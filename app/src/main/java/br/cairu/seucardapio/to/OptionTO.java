package br.cairu.seucardapio.to;

public class OptionTO {

    private int id;
    private String name;
    private String value;
    private Double price;

    public OptionTO(int id, String name, String value, Double price) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
