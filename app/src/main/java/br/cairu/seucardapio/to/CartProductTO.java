package br.cairu.seucardapio.to;

import java.util.List;

public class CartProductTO {

    private int id;
    private String name;
    private String description;
    private Double price;
    private List<OptionSelectedTO> additionalOptions;
    private String observation;

    public CartProductTO(int id, String name, String description, Double price, List<OptionSelectedTO> additionalOptions, String observation) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.additionalOptions = additionalOptions;
        this.observation = observation;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<OptionSelectedTO> getAdditionalOptions() {
        return additionalOptions;
    }

    public void setAdditionalOptions(List<OptionSelectedTO> additionalOptions) {
        this.additionalOptions = additionalOptions;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }
}
