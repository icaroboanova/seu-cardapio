package br.cairu.seucardapio.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import br.cairu.seucardapio.to.EnderecoTO;

public class SpinnerAddressAdapter extends ArrayAdapter<EnderecoTO> implements Serializable {

    private static final long serialVersionUID = -2271056938795792407L;

    private Context context;
    private List<EnderecoTO> addressList;

    public SpinnerAddressAdapter(Context context, int textViewResourceId, List<EnderecoTO> addressList) {
        super(context, textViewResourceId, addressList);
        this.context = context;
        this.addressList = addressList;
    }

    @Override
    public int getCount(){
        return this.addressList.size();
    }

    @Override
    public EnderecoTO getItem(int position){
        return this.addressList.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);

        label.setText(this.addressList.get(position).getName());

        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(this.addressList.get(position).getName());

        return label;
    }

    public void refreshAddressList(List<EnderecoTO> addressList) {
        this.addressList = addressList;
        notifyDataSetChanged();
    }

}
