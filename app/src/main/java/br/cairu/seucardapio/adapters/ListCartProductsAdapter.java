package br.cairu.seucardapio.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;

import br.cairu.seucardapio.R;
import br.cairu.seucardapio.activities.CartActivity;
import br.cairu.seucardapio.applications.GlobalApplication;
import br.cairu.seucardapio.to.CartProductTO;
import br.cairu.seucardapio.to.ProductTO;

public class ListCartProductsAdapter extends BaseAdapter {

    private Context context;
    List<CartProductTO> listProducts;

    public ListCartProductsAdapter(Context context, List<CartProductTO> listProducts) {
        this.context = context;
        this.listProducts = listProducts;
    }

    @Override
    public int getCount() {
        return this.listProducts.size();
    }

    @Override
    public CartProductTO getItem(int position) {
        return this.listProducts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = View.inflate(this.context, R.layout.layout_cart_product, null);

        view.setTag(this.listProducts.get(position));

        TextView textViewCartProductTitle = (TextView) view.findViewById(R.id.cartProductTitle);
        TextView textViewCartProductDescription = (TextView) view.findViewById(R.id.cartProductDescription);
        ImageButton buttonRemoveProduct = (ImageButton) view.findViewById(R.id.removeCartProduct);

        textViewCartProductTitle.setText(this.listProducts.get(position).getName());
        textViewCartProductDescription.setText(this.listProducts.get(position).getDescription());
        buttonRemoveProduct.setTag(this.listProducts.get(position));

        buttonRemoveProduct.setOnClickListener((e) -> {
            CartProductTO cartProductTO = (CartProductTO) e.getTag();
            this.removeProduct(cartProductTO);
        });

        return view;
    }

    public void removeProduct(CartProductTO cartProductTO) {
        this.listProducts.remove(cartProductTO);
        notifyDataSetChanged();
        if(this.listProducts.isEmpty()) ((CartActivity)context).finish();
        ((CartActivity)context).refreshTotal();
    }

    public void removeProduct(int position) {
        this.listProducts.remove(position);
        notifyDataSetChanged();
        if(this.listProducts.isEmpty()) ((CartActivity)context).finish();
        ((CartActivity)context).refreshTotal();
    }

    public void refreshProducts(List<ProductTO> productList) {
        this.listProducts = listProducts;
        notifyDataSetChanged();
    }

    public List<CartProductTO> getListProducts() { return this.listProducts; }

}
