package br.cairu.seucardapio.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

import br.cairu.seucardapio.R;
import br.cairu.seucardapio.activities.SelectProductActivity;
import br.cairu.seucardapio.to.ProductTO;

public class ListProductsAdapter extends BaseAdapter {

    private final static String EXCHANGE = "R$: ";

    private Context context;
    List<ProductTO> productList;

    public ListProductsAdapter(Context context, List<ProductTO> productList) {
        this.context = context;
        this.productList = productList;
    }

    @Override
    public int getCount() {
        return this.productList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.productList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = View.inflate(this.context, R.layout.layout_product, null);

        view.setTag(this.productList.get(position));

        ImageView imageViewProductPicture = (ImageView) view.findViewById(R.id.productPicture);
        TextView textViewProductTitle = (TextView) view.findViewById(R.id.productTitle);
        TextView textViewProductDescription = (TextView) view.findViewById(R.id.productDescription);
        TextView textViewProductPrice = (TextView) view.findViewById(R.id.productPrice);

        Picasso.get().load(this.productList.get(position).getImageUrl()).into(imageViewProductPicture);
        textViewProductTitle.setText(this.productList.get(position).getName());
        textViewProductDescription.setText(this.productList.get(position).getDescription());
        textViewProductPrice.setText(EXCHANGE + this.productList.get(position).getPrice());

        view.setOnClickListener((e) -> {
            System.out.println(e);
            ProductTO selectedProduct = (ProductTO) e.getTag();
            Intent intent = new Intent(this.context, SelectProductActivity.class);
            intent.putExtra("selectedProduct", selectedProduct);
            this.context.startActivity(intent);
        });

        return view;
    }

    public void removeProduct(int position) {
        this.productList.remove(position);
        notifyDataSetChanged();
    }

    public void refreshProducts(List<ProductTO> productList) {
        this.productList = productList;
        notifyDataSetChanged();
    }

}
