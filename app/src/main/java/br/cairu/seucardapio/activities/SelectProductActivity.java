package br.cairu.seucardapio.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import br.cairu.seucardapio.R;
import br.cairu.seucardapio.applications.GlobalApplication;
import br.cairu.seucardapio.to.CartProductTO;
import br.cairu.seucardapio.to.ProductTO;

public class SelectProductActivity extends AppCompatActivity {

    private GlobalApplication globalApplication;

    private ProductTO product;

    private ImageView selectedProductImage;
    private TextView selectedProductTitle;
    private TextView selectedProductDescription;
    private TextView selectedProductPrice;
    private EditText selectProductObservation;
    private EditText selectProductQuantity;

    private final static String EXCHANGE = "R$: ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_product);

        globalApplication = (GlobalApplication) this.getApplication();

        Intent intent = getIntent();
        product = (ProductTO) intent.getSerializableExtra("selectedProduct");

        this.selectedProductImage = (ImageView) findViewById(R.id.selectProductImage);
        this.selectedProductTitle = (TextView) findViewById(R.id.selectProductTitle);
        this.selectedProductDescription = (TextView) findViewById(R.id.selectProductDescription);
        this.selectedProductPrice = (TextView) findViewById(R.id.selectProductPrice);
        this.selectProductObservation = (EditText) findViewById(R.id.selectProductObservationField);
        this.selectProductQuantity = (EditText) findViewById(R.id.selectProductQuantityField);

        Picasso.get().load(this.product.getImageUrl()).into(this.selectedProductImage);
        this.selectedProductTitle.setText(this.product.getName());
        this.selectedProductDescription.setText(this.product.getDescription());
        this.selectedProductPrice.setText(EXCHANGE + this.product.getPrice());
        this.selectProductQuantity.setText("1");
    }

    public void cancel(View view) {
        finish();
    }

    public void confirm(View view) {
        int id = this.product.getId();
        String name = this.product.getName();
        String description = this.product.getDescription();
        Double price = this.product.getPrice();
        String observation = this.selectProductObservation.getText().toString();
        int quantity = Integer.parseInt(this.selectProductQuantity.getText().toString());
        while(quantity > 0) {
            CartProductTO cartProduct = new CartProductTO(id, name, description, price, null, observation);
            globalApplication.putProduct(cartProduct);
            quantity--;
        }
        finish();
    }

}