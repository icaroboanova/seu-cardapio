package br.cairu.seucardapio.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import br.cairu.seucardapio.R;
import br.cairu.seucardapio.adapters.SpinnerAddressAdapter;
import br.cairu.seucardapio.to.EnderecoTO;

public class NewAddressActivity extends AppCompatActivity {

    private EditText name;
    private EditText cep;
    private EditText street;
    private EditText neighborhood;
    private EditText city;
    private EditText number;
    private EditText complement;

    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_address);

        this.prefs = this.getSharedPreferences(
                "br.cairu.seucardapio", Context.MODE_PRIVATE);

        name = (EditText) findViewById(R.id.addressName);
        cep = (EditText) findViewById(R.id.addressCep);
        street = (EditText) findViewById(R.id.addressStreet);
        neighborhood = (EditText) findViewById(R.id.addressNeighborhood);
        city = (EditText) findViewById(R.id.addressCity);
        number = (EditText) findViewById(R.id.addressNumber);
        complement = (EditText) findViewById(R.id.addressComplement);
    }

    public void addAddressToUser(View view) {
        List<EnderecoTO> enderecos = getAddressFromCache();
        enderecos.add(new EnderecoTO(cep.getText().toString(), street.getText().toString(),
                neighborhood.getText().toString(), city.getText().toString(), number.getText().toString(),
                complement.getText().toString(), name.getText().toString()));
        this.prefs.edit().putString("enderecos", new Gson().toJson(enderecos)).apply();
        CartActivity.refreshAddress();
        finish();
    }

    private List<EnderecoTO> getAddressFromCache() {
        List<EnderecoTO> enderecos;
        String enderecosJson = this.prefs.getString("enderecos", "[]");
        enderecos = new Gson().fromJson(enderecosJson, new TypeToken<List<EnderecoTO>>(){}.getType());
        return enderecos;
    }

    public void searchCep(View view) {
        if(cep.getText().length() == 8) {
            String url = String.format("https://viacep.com.br/ws/%s/json/", cep.getText());

            RequestQueue queue = Volley.newRequestQueue(this);

            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject cepData = new JSONObject(response);
                                street.setText(cepData.getString("logradouro"));
                                neighborhood.setText((cepData.getString("bairro")));
                                city.setText(cepData.getString("localidade"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println(error.getMessage());
                }
            });

            // Add the request to the RequestQueue.
            queue.add(stringRequest);
        } else {
            Toast toast = Toast.makeText(this, "Falha ao buscar CEP", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

}