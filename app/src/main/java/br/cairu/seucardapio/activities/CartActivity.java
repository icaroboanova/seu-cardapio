package br.cairu.seucardapio.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import br.cairu.seucardapio.R;
import br.cairu.seucardapio.adapters.ListCartProductsAdapter;
import br.cairu.seucardapio.adapters.SpinnerAddressAdapter;
import br.cairu.seucardapio.applications.GlobalApplication;
import br.cairu.seucardapio.to.CartProductTO;
import br.cairu.seucardapio.to.EnderecoTO;
import br.cairu.seucardapio.to.FreteTO;
import br.cairu.seucardapio.to.ProductTO;
import br.cairu.seucardapio.utils.CardapioUtil;

public class CartActivity extends AppCompatActivity {

    private final static String EXCHANGE = "R$: ";

    private GlobalApplication globalApplication;
    private static SharedPreferences prefs;

    private static List<EnderecoTO> addressList;
    private Spinner addressListSpinner;
    private static SpinnerAddressAdapter spinnerAddressAdapter;
    private EnderecoTO addressSelected;

    private ListView listProductsCart;
    private ListCartProductsAdapter listCartProductsAdapter;

    private List<CartProductTO> listProducts;

    private TextView total;
    private FreteTO freteSelecionado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        globalApplication = (GlobalApplication) this.getApplication();

        listProducts = globalApplication.getProducts();

        prefs = getSharedPreferences(
                "br.cairu.seucardapio", Context.MODE_PRIVATE);

        String enderecosJson = prefs.getString("enderecos", "[]");
        addressList = new Gson().fromJson(enderecosJson, new TypeToken<List<EnderecoTO>>(){}.getType());

        if(addressList != null) {
            addressList = CardapioUtil.removeEmptyAddress(addressList);
        }

        addressListSpinner = (Spinner) findViewById(R.id.addressList);

        spinnerAddressAdapter = new SpinnerAddressAdapter(this, android.R.layout.simple_spinner_item, this.addressList);

        addressListSpinner.setAdapter(spinnerAddressAdapter);

        this.addressListSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                addressSelected = spinnerAddressAdapter.getItem(position);
                consultarFrete(addressSelected.getNeighborhood());
                refreshTotal();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });

        this.listProductsCart = (ListView) findViewById(R.id.listProductsCart);

        this.listCartProductsAdapter = new ListCartProductsAdapter(CartActivity.this, listProducts);

        this.listProductsCart.setAdapter(this.listCartProductsAdapter);

        this.total = (TextView) findViewById(R.id.textViewPrice);

        refreshTotal();
    }

    public void addAddress(View view) {
        Intent intent = new Intent(this, NewAddressActivity.class);
        this.startActivity(intent);
    }

    public static void refreshAddress() {
        refreshAddressFromCache();
        if(addressList != null) {
            addressList = CardapioUtil.removeEmptyAddress(addressList);
            spinnerAddressAdapter.refreshAddressList(addressList);
        }
    }

    private static void refreshAddressFromCache() {
        String enderecosJson = prefs.getString("enderecos", "[]");
        addressList = new Gson().fromJson(enderecosJson, new TypeToken<List<EnderecoTO>>(){}.getType());
    }

    public void refreshTotal() {
        BigDecimal total = BigDecimal.valueOf(0D);
        for(CartProductTO cartProductTO : this.listProducts) {
            total = total.add(BigDecimal.valueOf(cartProductTO.getPrice()));
        }
        if(freteSelecionado != null) {
            total = total.add(BigDecimal.valueOf(freteSelecionado.getPrice()));
        }
        this.total.setText(EXCHANGE + total.toString());
    }

    public void finishOrder(View view) {
        EnderecoTO addressOrder = addressSelected;
        if(addressOrder == null) addressOrder = (EnderecoTO) this.addressListSpinner.getSelectedItem();
        if(addressOrder != null && freteSelecionado != null) {
            callRestaurant();
        } else {
            mostrarMensagem("Selecione um endereço primeiro!");
        }
    }

    private void callRestaurant() {
        String url = "https://api.whatsapp.com/send?phone=" + "+5571983363359";
        url += "&text=";
        url += "Olá! Vim através do aplicativo SeuCardapio, meu pedido é: \n";
        for(CartProductTO cartProductTO : this.listProducts) {
            url += cartProductTO.getName();
            url += "\n";
            url += cartProductTO.getObservation();
            url += "\n";
        }
        url += "Para o bairro: ";
        url += freteSelecionado.getNeighborhood();
        url += "\n";
        url += "O total foi de: " + this.total.getText();
        try {
            PackageManager pm = this.getPackageManager();
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } catch (PackageManager.NameNotFoundException e) {
            mostrarMensagem("Por favor, instale o Whatsapp no seu dispositivo para prosseguir.");
        }
    }

    private void consultarFrete(String bairro) {
        for(FreteTO frete : globalApplication.getListNeighborhoods()) {
            if(compararStrings(bairro, frete.getNeighborhood())) {
                freteSelecionado = frete;
            }
        }
        if(freteSelecionado == null) {
            mostrarMensagem("Não entregamos neste local, por favor, selecione outro");
        }
    }

    private boolean compararStrings(String str1, String str2) {
        return CardapioUtil.removerAcentos(str1).equalsIgnoreCase(CardapioUtil.removerAcentos(str2));
    }

    private void mostrarMensagem(String mensagem) {
        Toast toast = Toast.makeText(this, mensagem, Toast.LENGTH_SHORT);
        toast.show();
    }

}