package br.cairu.seucardapio.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import br.cairu.seucardapio.R;
import br.cairu.seucardapio.adapters.ListProductsAdapter;
import br.cairu.seucardapio.applications.GlobalApplication;
import br.cairu.seucardapio.to.CartProductTO;
import br.cairu.seucardapio.to.FreteTO;
import br.cairu.seucardapio.to.ProductTO;
import br.cairu.seucardapio.utils.CardapioUtil;

public class MainActivity extends AppCompatActivity {

    private GlobalApplication globalApplication;

    private ImageView principalImage;

    private TabLayout menuTab;

    private ListView listProductsView;
    private ListProductsAdapter listProductsadapter;
    private List<CartProductTO> listCartProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        globalApplication = (GlobalApplication) this.getApplication();

        //carregar imagem principal
        this.principalImage = (ImageView) findViewById(R.id.principalImage);
        loadPrincipalImage();

        //carregar categorias
        this.menuTab = (TabLayout) findViewById(R.id.tabLayout);
        tabMount();

        //carregar produtos
        listProductsMount(null);

        this.listCartProducts = globalApplication.getProducts();

        //carregar enderecos de entrega
        //TODO carregar enderecos do banco
        listNeighborhoodsMount();
    }

    public void openCart(View view) {
        refreshListProducts();
        if(!this.listCartProducts.isEmpty()) {
            Intent intent = new Intent(this, CartActivity.class);
            this.startActivity(intent);
        } else {
            Toast toast = Toast.makeText(this, "Carrinho vazio", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    public void refreshListProducts() {
        this.listCartProducts = globalApplication.getProducts();
    }

    private void listNeighborhoodsMount() {
        globalApplication.getFirestore()
                .collection("bairros")
                .get()
                .addOnCompleteListener((result) -> {
                    if (result.isSuccessful()) {
                        List<FreteTO> fretes = globalApplication.getListNeighborhoods();
                        for (QueryDocumentSnapshot document : result.getResult()) {
                            FreteTO freteTO = new FreteTO(CardapioUtil.toString(document.get("nome")),
                                    Double.valueOf(CardapioUtil.toString(document.get("preco"))));
                            fretes.add(freteTO);
                        }
                        globalApplication.setListNeighborhoods(fretes);
                    } else {
                        Toast toast = Toast.makeText(this, "Não foi possível carregar os itens do cardápio", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                });
    }

    private void mountProducts(List<ProductTO> listProducts) {
        this.listProductsView = (ListView) findViewById(R.id.listProducts);

        this.listProductsadapter = new ListProductsAdapter(MainActivity.this, listProducts);

        this.listProductsView.setAdapter(this.listProductsadapter);
    }

    private void listProductsMount(Integer idTab) {
        if(idTab == null || idTab.compareTo(Integer.valueOf(0)) == 0 || idTab == 0) {
            globalApplication.getFirestore()
                    .collection("itens")
                    .get()
                    .addOnCompleteListener((result) -> {
                        if (result.isSuccessful()) {
                            List<ProductTO> listProducts = new ArrayList<ProductTO>();
                            for (QueryDocumentSnapshot document : result.getResult()) {
                                ProductTO productTO = new ProductTO(Integer.parseInt(CardapioUtil.toString(document.get("id"))),
                                        CardapioUtil.toString(document.get("urlImagem")),
                                        CardapioUtil.toString(document.get("titulo")),
                                        CardapioUtil.toString(document.get("descricao")),
                                        Double.valueOf(CardapioUtil.toString(document.get("preco"))));
                                listProducts.add(productTO);
                            }
                            mountProducts(listProducts);
                        } else {
                            Toast toast = Toast.makeText(this, "Não foi possível carregar os itens do cardápio", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    });
        } else {
            globalApplication.getFirestore()
                    .collection("itens")
                    .whereEqualTo("menuId", idTab.intValue())
                    .get()
                    .addOnCompleteListener((result) -> {
                        if (result.isSuccessful()) {
                            List<ProductTO> listProducts = new ArrayList<ProductTO>();
                            for (QueryDocumentSnapshot document : result.getResult()) {
                                ProductTO productTO = new ProductTO(Integer.parseInt(CardapioUtil.toString(document.get("id"))),
                                        CardapioUtil.toString(document.get("urlImagem")),
                                        CardapioUtil.toString(document.get("titulo")),
                                        CardapioUtil.toString(document.get("descricao")),
                                        Double.valueOf(CardapioUtil.toString(document.get("preco"))));
                                listProducts.add(productTO);
                            }
                            mountProducts(listProducts);
                        } else {
                            Toast toast = Toast.makeText(this, "Não foi possível carregar os itens do cardápio", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    });
        }
    }

    private void tabMount() {
        this.menuTab.addTab(this.menuTab.newTab().setText("Todos").setId(0));
        globalApplication.getFirestore()
                .collection("menu")
                .get()
                .addOnCompleteListener((result) -> {
                    if (result.isSuccessful()) {
                        for (QueryDocumentSnapshot document : result.getResult()) {
                            this.menuTab.addTab(this.menuTab.newTab().setText(CardapioUtil.toString(document.get("titulo"))).setId(Integer.parseInt(CardapioUtil.toString(document.get("id")))));
                        }
                    } else {
                        Toast toast = Toast.makeText(this, "Não foi possível carregar o menu", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                });
        this.menuTab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getId() == 0) {
                    listProductsMount(null);
                } else {
                    listProductsMount(tab.getId());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void loadPrincipalImage() {
        globalApplication.getFirestore()
                .collection("dados")
                .whereEqualTo("tipo", "imagemPrincipal")
                .get()
                .addOnCompleteListener((result) -> {
                    if (result.isSuccessful()) {
                        for (QueryDocumentSnapshot document : result.getResult()) {
                            Picasso.get().load(CardapioUtil.toString(document.get("logoUrl"))).into(this.principalImage);
                        }
                    } else {
                        Toast toast = Toast.makeText(this, "Não foi possível carregar a imagem principal", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                });
    }
}