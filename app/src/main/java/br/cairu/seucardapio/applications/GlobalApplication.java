package br.cairu.seucardapio.applications;

import android.app.Application;
import android.content.Context;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import br.cairu.seucardapio.to.CartProductTO;
import br.cairu.seucardapio.to.FreteTO;

public class GlobalApplication extends Application {

    private List<CartProductTO> cartProducts;
    private List<FreteTO> listNeighborhoods;
    private static Context context;
    private FirebaseFirestore firestore;

    @Override
    public void onCreate() {
        super.onCreate();
        cartProducts = new ArrayList<CartProductTO>();
        listNeighborhoods = new ArrayList<FreteTO>();
        firestore = FirebaseFirestore.getInstance();
        GlobalApplication.context = getApplicationContext();
    }

    public void putProduct(CartProductTO product) {
        cartProducts.add(product);
    }

    public void removeProduct(CartProductTO product) { cartProducts.remove(product); };

    public List<CartProductTO> getProducts() {
        return cartProducts;
    }

    public void setListNeighborhoods(List<FreteTO> listNeighborhoods) { this.listNeighborhoods = listNeighborhoods; }

    public List<FreteTO> getListNeighborhoods() { return this.listNeighborhoods; }

    public static Context getAppContext() { return GlobalApplication.context; }

    public FirebaseFirestore getFirestore() { return firestore; }

}
