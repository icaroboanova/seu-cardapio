package br.cairu.seucardapio.utils;

import android.os.Build;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import br.cairu.seucardapio.to.EnderecoTO;

public class CardapioUtil {

    public static boolean isNullOrEmpty(String str) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Objects.isNull(str) || str.isEmpty();
        } else {
            return str == null || str.isEmpty();
        }
    }

    public static List<EnderecoTO> removeEmptyAddress(List<EnderecoTO> addressList) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return addressList.stream().filter(address -> !isNullOrEmpty(address.getName())).collect(Collectors.toList());
        } else {
            List<EnderecoTO> retorno = new ArrayList<EnderecoTO>();
            for(EnderecoTO address : addressList) {
                if(!isNullOrEmpty(address.getName())) {
                    retorno.add(address);
                }
            }
            return retorno;
        }
    }

    public static String removerAcentos(String str) {
        return Normalizer.normalize(str, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
    }

    public static String toString(Object object) {
        return object != null ? object.toString() : "";
    }
}
